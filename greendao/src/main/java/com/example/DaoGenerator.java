package com.example;

import java.util.Properties;

import de.greenrobot.daogenerator.Entity;
import de.greenrobot.daogenerator.Property;
import de.greenrobot.daogenerator.Schema;

public class DaoGenerator {


    /**
     * Professor properties
     */
    private final String TABLE_PROFFESOR = "Professor";
    private final String PROFESSOR_ID = "professor_id";
    private final String PROFESSOR_NAME = "professor_name";
    private final String PROFESSOR_DEPARTMENT = "professor_department";
    private final String PROFESSOR_PHOTO = "professor_photo";
    private final String PROFESSOR_INFO = "professor_info";


    /**
     * Schedule properties
     */
    private final String TABLE_SCHEDULE = "Schedule";
    private final String SCHEDULE_ID = "schedule_id";
    private final String SCHEDULE_DAY = "schedule_day";// день
    private final String SCHEDULE_GROUP = "schedule_group"; // група
    private final String SCHEDULE_COURSE = "schedule_course";// курс
    private final String SCHEDULE_CLASS = "schedule_class";// пара
    private final String SCHEDULE_WEEK = "schedule_week";// тиждень
    private final String SCHEDULE_AUDITORY = "schedule_auditory";// аудиторія
    private final String SCHEDULE_SUBJECT = "schedule_subject";// предмет
    private final String SCHEDULE_PROFESSOR_JOIN_ID = "schedule_professor_id";

    private int appVersionCode = 1;

    Schema schema;

    public DaoGenerator(){
        Properties properties = new Properties();
    }

    public static void main(String[] args) throws Exception {
        new DaoGenerator().generate();
    }

    public void generate() throws Exception{
        schema = new Schema(appVersionCode,"com.mmh.pzks.domain.daogen");
        Entity professor = generateProfessor();
        generateSchedule(professor);

        new de.greenrobot.daogenerator.DaoGenerator().generateAll(schema,"./app/src/main/java");


    }

    public Entity generateProfessor(){
        Entity professor = schema.addEntity(TABLE_PROFFESOR);
        professor.addIdProperty().autoincrement().unique();
        professor.addStringProperty(PROFESSOR_NAME);
        professor.addStringProperty(PROFESSOR_DEPARTMENT);
        professor.addStringProperty(PROFESSOR_INFO);
        professor.addIntProperty(PROFESSOR_PHOTO).notNull();
        return professor;
    }

    public void generateSchedule(Entity professor){
        Entity schedule = schema.addEntity(TABLE_SCHEDULE);
        schedule.addIdProperty();
        Property professorID = schedule.addLongProperty(SCHEDULE_PROFESSOR_JOIN_ID).notNull().getProperty();
        professor.addToMany(schedule,professorID);
        schedule.addToOne(professor,professorID);
        schedule.addStringProperty(SCHEDULE_DAY);
        schedule.addStringProperty(SCHEDULE_GROUP);
        schedule.addIntProperty(SCHEDULE_COURSE).notNull();
        schedule.addIntProperty(SCHEDULE_CLASS).notNull();
        schedule.addIntProperty(SCHEDULE_WEEK).notNull();
        schedule.addStringProperty(SCHEDULE_AUDITORY);
        schedule.addStringProperty(SCHEDULE_SUBJECT);
    }
}
