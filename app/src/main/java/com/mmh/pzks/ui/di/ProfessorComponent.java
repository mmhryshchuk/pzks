package com.mmh.pzks.ui.di;

import com.mmh.pzks.core.di.scope.PerActivity;
import com.mmh.pzks.ui.screens.schedule.ProfessorActivity;
import com.mmh.pzks.ui.screens.schedule.ProfessorFragment;
import com.mmh.pzks.ui.screens.schedule.ProfessorPresenter;
import com.mmh.pzks.ui.screens.schedule.ProfessorPresenterImpl;


import dagger.Module;
import dagger.Provides;
import dagger.Subcomponent;

/**
 * Created by mmhryshchuk on 25.08.16.
 */
@Subcomponent(modules = ProfessorComponent.ProfessorModule.class)
@PerActivity
public interface ProfessorComponent  {

    void inject(ProfessorFragment professorFragment);

    @Module
    class ProfessorModule{

        @Provides
        @PerActivity
        ProfessorPresenter providePresenter(){
            return new ProfessorPresenterImpl();
        }
    }
}
