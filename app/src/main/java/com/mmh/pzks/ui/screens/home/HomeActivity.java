package com.mmh.pzks.ui.screens.home;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import com.mmh.pzks.R;
import com.mmh.pzks.core.android.App;
import com.mmh.pzks.core.android.BaseActivity;
import com.mmh.pzks.domain.repository.controller.ProfessorController;
import com.mmh.pzks.ui.di.HomeComponent;

import javax.inject.Inject;

import butterknife.ButterKnife;

public class HomeActivity extends BaseActivity  {

    @Inject
    ProfessorController controller;
    @Inject
    HomePresenter homePresenter;

    public static void start(Activity activity){
        Intent intent = new Intent(activity, HomeActivity.class);
        activity.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        ButterKnife.bind(this);
        setupDagger();
        replaceFragment(homePresenter,new HomeFragment());
    }

    private void setupDagger(){
        App.getApp(this).getAppComponent().plus(new HomeComponent.HomeModule()).inject(this);
    }
}
