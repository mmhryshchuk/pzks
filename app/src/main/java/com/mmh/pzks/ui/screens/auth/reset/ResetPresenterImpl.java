package com.mmh.pzks.ui.screens.auth.reset;

import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.mmh.pzks.core.mvp.BasePresenter;
import com.mmh.pzks.domain.entities.AuthDvo;
import com.mmh.pzks.ui.screens.auth.AuthActivity;

/**
 * Created by mmhryshchuk on 17.08.16.
 */
public class ResetPresenterImpl extends BasePresenter<ResetView> implements ResetPresenter {

    FirebaseAuth auth;

    public ResetPresenterImpl() {
        auth = FirebaseAuth.getInstance();
    }
    @Override
    public void onResetClick(AuthDvo authDvo, AuthActivity authActivity) {
        if (getView() == null) return;
        auth.sendPasswordResetEmail(authDvo.getEmail()).addOnCompleteListener(task -> {
            if (task.isSuccessful()){
                Toast.makeText(authActivity,"We have send you instruction to reset password",Toast.LENGTH_LONG).show();
                authActivity.openLoginScreen();
            }else {
                getView().showErrorMessage();
            }
        });
    }

}
