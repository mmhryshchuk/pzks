package com.mmh.pzks.ui.screens.schedule;




/**
 * Created by mmhryshchuk on 29.07.16.
 */
public interface ProfessorView {
    void showProfessorRecycler();
    void showStudentGroupRecycler();
    void addProfessorData(/*List<AuthorsDvo> authorsDvoList*/);
    void addStudentGroupData(/*List<CategoryDvo> categoryDvoList*/);

    void showProfessorScreen(/*AuthorsDvo authorsDvo*/);
    void showGroupScreen(/*CategoryDvo categoryDvo*/);
}
