package com.mmh.pzks.ui.screens.auth.reset;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.mmh.pzks.R;
import com.mmh.pzks.core.android.App;
import com.mmh.pzks.core.android.MVPBaseFragment;
import com.mmh.pzks.core.rx.SimpleSubscriber;
import com.mmh.pzks.core.rx.TextWatcherObservable;
import com.mmh.pzks.core.utils.EmailValidator;
import com.mmh.pzks.core.utils.StringUtils;
import com.mmh.pzks.domain.entities.AuthDvo;
import com.mmh.pzks.ui.screens.auth.AuthActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by mmhryshchuk on 17.08.16.
 */
public class ResetFragment  extends MVPBaseFragment<ResetPresenter> implements ResetView  {

    @BindView(R.id.reset_fragment_btn_reset)
    TextView vReset;
    @BindView(R.id.reset_fragment_email)
    EditText vEmail;

    AuthDvo authDvo;
    CompositeSubscription subscription = new CompositeSubscription();


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.reset_fragment,container,false);
        ButterKnife.bind(this,view);
        return view;
    }


    @Override
    public void onStart() {
        super.onStart();
        getPresenter().attachView(this);
        enableReset(false);
        subscription.add(TextWatcherObservable.create(vEmail).subscribe(new SimpleSubscriber<String>(){
            @Override
            public void onNext(String item) {
                super.onNext(item);
                validate();
            }
        }));
    }

    @Override
    public void onStop() {
        super.onStop();
        getPresenter().detachView();
    }

    public void validate(){
        boolean isAllFine = true;
        String email = vEmail.getText().toString();
        if (StringUtils.isNullEmpty(email) ){
            isAllFine = false;
        }

        if (!EmailValidator.isValid(email)){
            vEmail.setTextColor(ContextCompat.getColor(getActivity(), R.color.colorTextError));
            isAllFine = false;
        }else {
            vEmail.setTextColor(ContextCompat.getColor(getActivity(), R.color.colorAccent));

        }

        if (isAllFine){
            enableReset(true);
        } else {
            enableReset(false);
        }
    }

    @Override
    public void showErrorMessage() {
        Toast.makeText(getActivity(),"Something go wrong! Don`t give up!!",Toast.LENGTH_SHORT).show();
    }

    @Override
    public void enableReset(boolean enable) {
        vReset.setEnabled(enable);
    }

    @OnClick(R.id.reset_fragment_btn_reset)
    public void onResetClick(){
        String email = vEmail.getText().toString();
        authDvo = new AuthDvo(email,"");
        getPresenter().onResetClick(authDvo, (AuthActivity) getActivity());
    }


}
