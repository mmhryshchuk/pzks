package com.mmh.pzks.ui.screens.auth.login;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.mmh.pzks.R;
import com.mmh.pzks.core.android.App;
import com.mmh.pzks.core.android.MVPBaseFragment;
import com.mmh.pzks.core.rx.SimpleSubscriber;
import com.mmh.pzks.core.rx.TextWatcherObservable;
import com.mmh.pzks.core.utils.EmailValidator;
import com.mmh.pzks.core.utils.StringUtils;
import com.mmh.pzks.domain.entities.AuthDvo;
import com.mmh.pzks.ui.di.AuthComponent;
import com.mmh.pzks.ui.screens.auth.AuthActivity;
import com.mmh.pzks.ui.screens.home.HomeActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by mmhryshchuk on 17.08.16.
 */
public class LoginFragment extends MVPBaseFragment<LoginPresenter> implements LoginView {

    @BindView(R.id.login_fragment_btn_login)
    TextView vLogin;
    @BindView(R.id.login_fragment_email)
    EditText vEmail;
    @BindView(R.id.login_fragment_password)
    EditText vPassword;

    AuthDvo authDvo;
    CompositeSubscription subscription = new CompositeSubscription();

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.login_fragment,container,false);
        ButterKnife.bind(this,view);
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        getPresenter().attachView(this);
        enableLogin(false);
        subscription.add(TextWatcherObservable.create(vEmail).subscribe(new SimpleSubscriber<String>(){
            @Override
            public void onNext(String item) {
                super.onNext(item);
                validate();
            }
        }));
        subscription.add(TextWatcherObservable.create(vPassword).subscribe(new SimpleSubscriber<String>(){
            @Override
            public void onNext(String item) {
                super.onNext(item);
                validate();
            }
        }));

    }

    @Override
    public void onStop() {
        super.onStop();
        getPresenter().detachView();
    }

    public void validate(){
        boolean isAllFine = true;
        String email = vEmail.getText().toString();
        String password = vPassword.getText().toString();
        if (StringUtils.isNullEmpty(email) ){
            isAllFine = false;
        }
        if (StringUtils.isNullEmpty(password) ){
            isAllFine = false;
        }
        if (!EmailValidator.isValid(email)){
            vEmail.setTextColor(ContextCompat.getColor(getActivity(), R.color.colorTextError));
            isAllFine = false;
        }else {
            vEmail.setTextColor(ContextCompat.getColor(getActivity(), R.color.colorAccent));

        }
        if (password.length() < 6){
            isAllFine = false;
            vPassword.setTextColor(ContextCompat.getColor(getActivity(), R.color.colorTextError));
        }else{
            vPassword.setTextColor(ContextCompat.getColor(getActivity(), R.color.colorAccent));
        }
        if (isAllFine){
            enableLogin(true);
        } else {
            enableLogin(false);
        }
    }

    @Override
    public void showErrorMessage() {
        Toast.makeText(getActivity(),"Something go wrong! Don`t give up!!",Toast.LENGTH_SHORT).show();
    }

    @Override
    public void enableLogin(boolean enable) {
        vLogin.setEnabled(enable);
    }

    @Override
    public void openHome() {
        HomeActivity.start(getActivity());
    }

    @OnClick(R.id.login_fragment_btn_login)
    public void onLoginCLick(){
        String email = vEmail.getText().toString();
        String password = vPassword.getText().toString();
        authDvo = new AuthDvo(email,password);
        getPresenter().onLoginClick(authDvo, (AuthActivity) getActivity());
    }

    @OnClick(R.id.login_fragment_btn_register)
    public void onRegisterClick(){
        getPresenter().onRegisterClick((AuthActivity) getActivity());
    }

    @OnClick(R.id.login_fragment_btn_reset)
    public void onResetClick(){
        getPresenter().onResetClick((AuthActivity) getActivity());
    }

    @OnClick(R.id.login_fragment_btn_anon)
    public void onAnonClick(){
        getPresenter().onAnonClick();
    }
}
