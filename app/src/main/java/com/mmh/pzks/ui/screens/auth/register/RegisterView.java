package com.mmh.pzks.ui.screens.auth.register;

import com.mmh.pzks.domain.entities.AuthDvo;

/**
 * Created by mmhryshchuk on 17.08.16.
 */
public interface RegisterView  {


    void showErrorMassage();
    void enableRegister(boolean enable);

}
