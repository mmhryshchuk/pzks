package com.mmh.pzks.ui.screens.auth.reset;

/**
 * Created by mmhryshchuk on 17.08.16.
 */
public interface ResetView {
    void showErrorMessage();
    void enableReset(boolean enable);
}
