package com.mmh.pzks.ui.screens.schedule;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.mmh.pzks.R;
import com.mmh.pzks.core.android.App;
import com.mmh.pzks.core.android.MVPBaseFragment;
import com.mmh.pzks.ui.di.ProfessorComponent;
import com.mmh.pzks.ui.screens.schedule.adapters.ProfessorAdapter;
import com.mmh.pzks.ui.screens.schedule.adapters.StudentAdapter;
import com.mmh.pzks.ui.screens.schedule.adapters.ViewPagerAdapter;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;


/**
 * Created by mmhryshchuk on 28.07.16.
 */
public class ProfessorFragment extends MVPBaseFragment<ProfessorPresenter> implements ProfessorView, ProfessorAdapter.OnProfessorClickListener,StudentAdapter.OnStudentClickListener{

    @BindView(R.id.catalog_home_fragment_categories_recycler)
    RecyclerView vGroupRecycler;
    @BindView(R.id.catalog_home_fragment_authors_recycler)
    RecyclerView vProfessorRecycler;
    @BindView(R.id.catalog_home_fragment_view_pager)
    ViewPager vViewPager;
    @BindView(R.id.view_pager_authors)
    FrameLayout vProfessorFrame;
    @BindView(R.id.view_pager_categories)
    FrameLayout vGroupFrame;
    @BindView(R.id.authors_progress)
    ProgressBar vAuthorsProgress;
    @BindView(R.id.categories_progress)
    ProgressBar vCategoriesProgress;
    @BindView(R.id.toolbar)
    Toolbar vToolbar;


    ProfessorAdapter professorAdapter;
    StudentAdapter studentAdapter;
    ViewPagerAdapter viewPagerAdapter;
    ProfessorAdapter.OnProfessorClickListener listener;

    @Inject
    ProfessorPresenter professorPresenter;

    int icon = R.drawable.ic_chevron_left_white;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.professor_fragment,container,false);
        ButterKnife.bind(this,view);
        setupToolbar(vToolbar,"Розклад",icon,view1 -> getActivity().onBackPressed());
        return view;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setupDagger();
        attachPresenter(professorPresenter);
    }

    @Override
    public void onStop() {
        super.onStop();
        professorPresenter.detachView();
    }

    @Override
    public void onStart() {
        super.onStart();
        professorPresenter.attachView(this);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initAuthorsRecycler();
        initCategoriesRecycler();
        initViewPager();
        showStudentGroupRecycler();
    }

    private void setupDagger(){
        App.getApp(getActivity()).getAppComponent().plus(new ProfessorComponent.ProfessorModule()).inject(this);
    }

    public void initCategoriesRecycler(){
        studentAdapter = new StudentAdapter(getActivity(),this);
        vGroupRecycler.setLayoutManager(new LinearLayoutManager(getActivity()));
//        studentAdapter.setCategoryDvoList(createCategories());
        vGroupRecycler.setAdapter(studentAdapter);

    }

    public void initAuthorsRecycler(){
        professorAdapter = new ProfessorAdapter(getActivity(),this);
        vProfessorRecycler.setLayoutManager(new LinearLayoutManager(getActivity()));
//        professorAdapter.setAuthorsDvoList(createAuthors(getActivity()));
        vProfessorRecycler.setAdapter(professorAdapter);
    }

    public void initViewPager(){
        viewPagerAdapter = new ViewPagerAdapter();
        List<FrameLayout> frames = new ArrayList<>();
        List<String> titles = new ArrayList<>();
        frames.add(vGroupFrame);
        frames.add(vProfessorFrame);
        titles.add("Групи");
        titles.add("Викладачі");
        viewPagerAdapter.addFrame(frames,titles);
        vViewPager.setAdapter(viewPagerAdapter);
        vViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                if (position == 0){
                    showStudentGroupRecycler();
                }else {
                    showProfessorRecycler();
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }



    @Override
    public void showProfessorRecycler() {
        vViewPager.setCurrentItem(1);

    }

    @Override
    public void showStudentGroupRecycler() {
        vViewPager.setCurrentItem(0);
    }

    @Override
    public void addProfessorData() {

    }

    @Override
    public void addStudentGroupData() {

    }

    @Override
    public void showProfessorScreen() {

    }

    @Override
    public void showGroupScreen() {

    }


    @Override
    public void onAuthorClick() {

    }

    @Override
    public void onGroupClick() {

    }
}
