package com.mmh.pzks.ui.screens.auth.register;

import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.mmh.pzks.core.mvp.BasePresenter;
import com.mmh.pzks.domain.entities.AuthDvo;
import com.mmh.pzks.ui.screens.auth.AuthActivity;

/**
 * Created by mmhryshchuk on 17.08.16.
 */
public class RegisterPresenterImpl extends BasePresenter<RegisterView> implements RegisterPresenter {

    FirebaseAuth auth;

    public RegisterPresenterImpl() {
        auth = FirebaseAuth.getInstance();
    }

    @Override
    public void onRegisterClick(AuthDvo authDvo,AuthActivity authActivity) {
        if (getView() == null) return;
        auth.createUserWithEmailAndPassword(authDvo.getEmail(),authDvo.getPassword()).addOnCompleteListener(task -> {
           if (task.isSuccessful()){
               authActivity.openNextScreen();
           } else {
               getView().showErrorMassage();
               Toast.makeText(authActivity, String.format("%s", task.getException()),Toast.LENGTH_SHORT).show();
           }
        });
    }

    @Override
    public void onForgotClick(AuthActivity authActivity) {
        authActivity.openResetScreen();
    }

    @Override
    public void onLoginClick(AuthActivity authActivity) {
        authActivity.openLoginScreen();
    }
}
