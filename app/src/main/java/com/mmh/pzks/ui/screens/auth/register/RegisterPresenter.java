package com.mmh.pzks.ui.screens.auth.register;

import com.mmh.pzks.core.mvp.Presenter;
import com.mmh.pzks.domain.entities.AuthDvo;
import com.mmh.pzks.ui.screens.auth.AuthActivity;

/**
 * Created by mmhryshchuk on 17.08.16.
 */
public interface RegisterPresenter  extends Presenter<RegisterView> {

    void onRegisterClick(AuthDvo authDvo,AuthActivity authActivity);
    void onForgotClick(AuthActivity authActivity);
    void onLoginClick(AuthActivity authActivity);
}
