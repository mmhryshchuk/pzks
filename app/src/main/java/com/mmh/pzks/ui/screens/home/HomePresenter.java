package com.mmh.pzks.ui.screens.home;

import com.mmh.pzks.core.mvp.Presenter;

/**
 * Created by mmhryshchuk on 05.09.16.
 */
public interface HomePresenter extends Presenter<HomeView> {

    void onScheduleClick();
    void onNewClick();
    void onAboutClick();
    void onLogout();

}
