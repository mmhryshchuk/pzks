package com.mmh.pzks.ui.di;

import com.mmh.pzks.core.di.scope.PerActivity;
import com.mmh.pzks.ui.screens.auth.AuthActivity;
import com.mmh.pzks.ui.screens.auth.login.LoginFragment;
import com.mmh.pzks.ui.screens.auth.login.LoginPresenter;
import com.mmh.pzks.ui.screens.auth.login.LoginPresenterImpl;
import com.mmh.pzks.ui.screens.auth.register.RegisterPresenter;
import com.mmh.pzks.ui.screens.auth.register.RegisterPresenterImpl;
import com.mmh.pzks.ui.screens.auth.reset.ResetPresenter;
import com.mmh.pzks.ui.screens.auth.reset.ResetPresenterImpl;

import dagger.Module;
import dagger.Provides;
import dagger.Subcomponent;

/**
 * Created by mmhryshchuk on 18.08.16.
 */

@Subcomponent(modules = AuthComponent.AuthModule.class)
@PerActivity
public interface AuthComponent {

    void inject(AuthActivity authActivity);


    @Module
    class AuthModule{

        @Provides
        @PerActivity
        LoginPresenter provideLoginPresenter(){
            return new LoginPresenterImpl();
        }

        @Provides
        @PerActivity
        RegisterPresenter provideRegisterPresenter(){
            return new RegisterPresenterImpl();
        }

        @Provides
        @PerActivity
        ResetPresenter provideResetPresenter(){
            return new ResetPresenterImpl();
        }
    }
}
