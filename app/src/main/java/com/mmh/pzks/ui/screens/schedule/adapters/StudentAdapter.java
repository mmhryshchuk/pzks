package com.mmh.pzks.ui.screens.schedule.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mmh.pzks.R;
import com.mmh.pzks.ui.screens.schedule.adapters.view_holders.StudentViewHolder;

/**
 * Created by mmhryshchuk on 28.07.16.
 */
public class StudentAdapter extends RecyclerView.Adapter<StudentViewHolder> implements View.OnClickListener {

    LayoutInflater inflater;
    OnStudentClickListener listener;
    Context context;
//    List<CategoryDvo> categoryDvoList = new ArrayList<>();

    public StudentAdapter(Context context, OnStudentClickListener listener) {
        this.listener = listener;
        this.context = context;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public StudentViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.shcedule_student_item,parent,false);
        return new StudentViewHolder(view);
    }

    @Override
    public void onBindViewHolder(StudentViewHolder holder, int position) {
//        CategoryDvo item = getCategoryDvoList().get(position);
//        holder.bind(context,item);
//        holder.itemView.setOnClickListener(this);
//        holder.itemView.setTag(position);
    }

    @Override
    public int getItemCount() {
        return 0;
    }

//    public List<CategoryDvo> getCategoryDvoList(){
//        return categoryDvoList;
//    }

//    public void setCategoryDvoList(List<CategoryDvo> categoryDvos){
//        categoryDvoList.clear();
//        categoryDvoList.addAll(categoryDvos);
//        notifyDataSetChanged();
//    }

    @Override
    public void onClick(View view) {
        int position = (int) view.getTag();
//        listener.onStudentGroupItemClick(getCategoryDvoList().get(position));
    }

    public interface OnStudentClickListener{
        void onGroupClick();
    }
}
