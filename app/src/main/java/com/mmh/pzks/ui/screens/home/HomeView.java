package com.mmh.pzks.ui.screens.home;

/**
 * Created by mmhryshchuk on 05.09.16.
 */
public interface HomeView {

    void openNews();
    void openAbout();
    void openSchedule();
    void openLogin();
}
