package com.mmh.pzks.ui.screens.schedule.adapters.view_holders;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.mmh.pzks.R;

import butterknife.BindView;
import butterknife.ButterKnife;


/**
 * Created by mmhryshchuk on 28.07.16.
 */
public class ProfessorViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.schedule_professor_item_image)
    ImageView vImage;
    @BindView(R.id.schedule_professor_item_name)
    TextView vName;
    @BindView(R.id.catalog_author_item_text_btn)
    TextView vTextBtn;

    public ProfessorViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this,itemView);
    }
//    public void bind(Context context, AuthorsDvo authorsDvo){
//        vName.setText(authorsDvo.getName());
//        vSecondaryText.setText(authorsDvo.getSecondaryText());
//        boolean isImage = !StringUtils.isNullEmpty(authorsDvo.getImage());
//        if (isImage){
//            ImageLoader.loadUser(vImage, authorsDvo.getImage());
//        }else {
//            vTextBtn.setText(StringUtils.generateShortName(authorsDvo.getFirstName(), authorsDvo.getLastName()));
//        }
//
//        vTextBtn.setVisibility(isImage ? View.GONE : View.VISIBLE);
//        vImage.setVisibility(isImage ? View.VISIBLE : View.GONE);
//    }
}
