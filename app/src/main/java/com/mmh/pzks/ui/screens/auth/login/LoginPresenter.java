package com.mmh.pzks.ui.screens.auth.login;

import com.mmh.pzks.core.mvp.Presenter;
import com.mmh.pzks.domain.entities.AuthDvo;
import com.mmh.pzks.ui.screens.auth.AuthActivity;

/**
 * Created by mmhryshchuk on 17.08.16.
 */
public interface LoginPresenter extends Presenter<LoginView> {
    void onLoginClick(AuthDvo authDvo, AuthActivity authActivity);
    void onRegisterClick(AuthActivity authActivity);
    void onResetClick(AuthActivity authActivity);
    void onAnonClick();
}
