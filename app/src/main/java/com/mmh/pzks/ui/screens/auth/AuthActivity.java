package com.mmh.pzks.ui.screens.auth;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;

import com.mmh.pzks.R;
import com.mmh.pzks.core.android.App;
import com.mmh.pzks.core.android.BaseActivity;
import com.mmh.pzks.ui.di.AuthComponent;
import com.mmh.pzks.ui.screens.auth.login.LoginFragment;
import com.mmh.pzks.ui.screens.auth.login.LoginPresenter;
import com.mmh.pzks.ui.screens.auth.register.RegisterFragment;
import com.mmh.pzks.ui.screens.auth.register.RegisterPresenter;
import com.mmh.pzks.ui.screens.auth.reset.ResetFragment;
import com.mmh.pzks.ui.screens.auth.reset.ResetPresenter;
import com.mmh.pzks.ui.screens.home.HomeActivity;

import javax.inject.Inject;

/**
 * Created by mmhryshchuk on 17.08.16.
 */
public class AuthActivity extends BaseActivity {

    @Inject
    RegisterPresenter registerPresenter;
    @Inject
    ResetPresenter resetPresenter;
    @Inject
    LoginPresenter loginPresenter;

    public static void start(Activity activity){
        Intent intent = new Intent(activity, AuthActivity.class);
        activity.startActivity(intent);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_auth);
        setupDagger();
        replaceFragment(registerPresenter,new RegisterFragment());

    }

    private void setupDagger(){
        App.getApp(this).getAppComponent().plus(new AuthComponent.AuthModule()).inject(this);
    }

    @Override
    public void onBackPressed() {
        Fragment currentFragment = getSupportFragmentManager().findFragmentById(R.id.fragment_container);
        if (currentFragment instanceof LoginFragment)
            replaceFragment(registerPresenter,new RegisterFragment());
        else if (currentFragment instanceof ResetFragment)
            replaceFragment(registerPresenter,new RegisterFragment());
    }

    public void openLoginScreen(){
        replaceFragment(loginPresenter,new LoginFragment());
    }

    public void openRegisterScreen(){
        replaceFragment(registerPresenter,new RegisterFragment());
    }

    public void openResetScreen(){
        replaceFragment(resetPresenter,new ResetFragment());
    }

    public void openNextScreen(){
        HomeActivity.start(this);
        finish();
    }
}
