package com.mmh.pzks.ui.screens.schedule.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mmh.pzks.R;
import com.mmh.pzks.ui.screens.schedule.adapters.view_holders.ProfessorViewHolder;


/**
 * Created by mmhryshchuk on 28.07.16.
 */
public class ProfessorAdapter extends RecyclerView.Adapter<ProfessorViewHolder> implements View.OnClickListener{

    Context context;
    LayoutInflater inflater;

//    List<AuthorsDvo> authorsDvoList = new ArrayList<>();
    OnProfessorClickListener listener;

    public ProfessorAdapter(Context context, OnProfessorClickListener listener) {
        this.context = context;
        this.listener = listener;
        inflater = LayoutInflater.from(context);
    }


    @Override
    public ProfessorViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.schedule_professor_item,parent,false);
        return new ProfessorViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ProfessorViewHolder holder, int position) {
//        AuthorsDvo item = getAuthorsDvoList().get(position);
//        holder.bind(context,item);
//        holder.itemView.setTag(position);
//        holder.itemView.setOnClickListener(this);
    }

    @Override
    public int getItemCount() {
        return 0;
    }

    @Override
    public void onClick(View view) {

    }

    //    public List<AuthorsDvo> getAuthorsDvoList(){
//        return authorsDvoList;
//    }
//
//    public void setAuthorsDvoList(List<AuthorsDvo> authorsDvos){
//        authorsDvoList.clear();
//        authorsDvoList.addAll(authorsDvos);
//        notifyDataSetChanged();
//    }
//
//    @Override
//    public void onClick(View view) {
//        int position = (int) view.getTag();
//        AuthorsDvo authorsDvo = getAuthorsDvoList().get(position);
//        listener.onProfessorItemClick(authorsDvo);
//    }
//
    public interface OnProfessorClickListener {
        void onAuthorClick();
    }
}
