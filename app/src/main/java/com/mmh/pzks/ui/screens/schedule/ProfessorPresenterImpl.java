package com.mmh.pzks.ui.screens.schedule;

import com.mmh.pzks.core.mvp.BasePresenter;

import javax.inject.Inject;



/**
 * Created by mmhryshchuk on 29.07.16.
 */
public class ProfessorPresenterImpl extends BasePresenter<ProfessorView> implements ProfessorPresenter {



    @Inject
    public ProfessorPresenterImpl() {

    }

    @Override
    protected void onViewAttached() {
        super.onViewAttached();
        getView().addProfessorData();
        getView().addStudentGroupData();
    }

    @Override
    public void onStudentGroupClick() {
        getView().showStudentGroupRecycler();
    }

    @Override
    public void onProfessorClick() {
        getView().showProfessorRecycler();
    }

    @Override
    public void onStudentGroupItemClick() {
        getView().showGroupScreen();
    }

    @Override
    public void onProfessorItemClick() {
        getView().showProfessorScreen();
    }
}
