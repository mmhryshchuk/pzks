package com.mmh.pzks.ui.screens.about;

import android.app.Activity;
import android.content.Intent;
import android.location.LocationProvider;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.mmh.pzks.R;
import com.mmh.pzks.core.android.App;
import com.mmh.pzks.core.android.BaseActivity;
import com.mmh.pzks.ui.di.AboutComponent;

import javax.inject.Inject;

import butterknife.ButterKnife;

/**
 * Created by mmhryshchuk on 05.09.16.
 */
public class AboutActivity extends FragmentActivity implements  OnMapReadyCallback {

    @Inject
    AboutPresenter aboutPresenter;

    private GoogleMap mMap;


    public static void start(Activity activity){
        Intent intent = new Intent(activity, AboutActivity.class);
        activity.startActivity(intent);
    }


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);
        ButterKnife.bind(this);
        setUpMapIfNeeded();
        setupDagger();
    }

    private void setupDagger(){
        App.getApp(this).getAppComponent().plus(new AboutComponent.AboutModule()).inject(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        setUpMapIfNeeded();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        setUpMap();
    }

    private void setUpMapIfNeeded() {
        if (mMap == null) {
            SupportMapFragment mapFragment = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.google_map));
            mapFragment.getMapAsync(this);
            if (mMap != null) {
                setUpMap();
            }
        }
    }

    private void setUpMap() {
        LatLng univer = new LatLng(48.268673, 25.912805);
        mMap.addMarker(new MarkerOptions().position(univer).title("9 корпус"));
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(univer,15.0f));
    }
}
