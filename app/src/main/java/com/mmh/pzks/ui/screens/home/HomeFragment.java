package com.mmh.pzks.ui.screens.home;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mmh.pzks.R;
import com.mmh.pzks.core.android.MVPBaseFragment;
import com.mmh.pzks.ui.screens.about.AboutActivity;
import com.mmh.pzks.ui.screens.auth.AuthActivity;
import com.mmh.pzks.ui.screens.schedule.ProfessorActivity;

import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by mmhryshchuk on 05.09.16.
 */
public class HomeFragment extends MVPBaseFragment<HomePresenter> implements HomeView {

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.home_fragment,container,false);
        ButterKnife.bind(this,view);
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        getPresenter().attachView(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        getPresenter().detachView();
    }

    @Override
    public void openNews() {

    }

    @Override
    public void openAbout() {
        AboutActivity.start(getActivity());
    }

    @Override
    public void openSchedule() {
        ProfessorActivity.start(getActivity());
    }

    @Override
    public void openLogin() {
        AuthActivity.start(getActivity());
        getActivity().finish();
    }


    @OnClick(R.id.home_fragment_logout_btn)
    public void logOut(){
        getPresenter().onLogout();
    }

    @OnClick(R.id.main_btn_map)
    public void onAboutClick(){
        getPresenter().onAboutClick();
    }

    @OnClick(R.id.main_btn_schedule)
    public void onScheduleClick(){
        getPresenter().onScheduleClick();
    }
}
