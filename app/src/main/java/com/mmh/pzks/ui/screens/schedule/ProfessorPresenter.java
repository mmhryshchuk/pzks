package com.mmh.pzks.ui.screens.schedule;


import com.mmh.pzks.core.mvp.Presenter;

/**
 * Created by mmhryshchuk on 29.07.16.
 */
public interface ProfessorPresenter extends Presenter<ProfessorView> {
    void onStudentGroupClick();
    void onProfessorClick();
    void onStudentGroupItemClick();
    void onProfessorItemClick();
}
