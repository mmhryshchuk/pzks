package com.mmh.pzks.ui.di;

import com.mmh.pzks.core.di.scope.PerActivity;
import com.mmh.pzks.ui.screens.about.AboutActivity;
import com.mmh.pzks.ui.screens.about.AboutPresenter;
import com.mmh.pzks.ui.screens.about.AboutPresenterImpl;

import dagger.Module;
import dagger.Provides;
import dagger.Subcomponent;

/**
 * Created by mmhryshchuk on 05.09.16.
 */
@Subcomponent(modules = AboutComponent.AboutModule.class)
@PerActivity
public interface AboutComponent {

    void inject(AboutActivity aboutActivity);

    @Module
    class AboutModule{

        @Provides
        @PerActivity
        AboutPresenter providePresenter(){
            return new AboutPresenterImpl();
        }
    }
}
