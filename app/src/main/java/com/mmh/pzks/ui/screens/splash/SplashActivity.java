package com.mmh.pzks.ui.screens.splash;

import android.os.Bundle;
import android.os.Handler;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.mmh.pzks.R;
import com.mmh.pzks.core.android.BaseActivity;
import com.mmh.pzks.ui.screens.auth.AuthActivity;
import com.mmh.pzks.ui.screens.home.HomeActivity;

import butterknife.ButterKnife;

public class SplashActivity extends BaseActivity {

    private static int SPLASH_TIME_OUT = 3000;
    private FirebaseAuth.AuthStateListener mAuthListener;
    FirebaseAuth auth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        ButterKnife.bind(this);
        auth = FirebaseAuth.getInstance();
        mAuthListener = firebaseAuth -> {
            FirebaseUser user = auth.getCurrentUser();
            if (user != null){
                startNewScreen(0);
            }else {
                startNewScreen(1);
            }

        };
    }

    @Override
    protected void onStart() {
        super.onStart();
        auth.addAuthStateListener(mAuthListener);
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (mAuthListener != null) {
            auth.removeAuthStateListener(mAuthListener);
        }
    }

    public void startNewScreen(int loggin){
        new Handler().postDelayed(() -> {
            if (loggin == 1){
                AuthActivity.start(SplashActivity.this);
            }else {
                HomeActivity.start(SplashActivity.this);
            }
            finish();
        }, SPLASH_TIME_OUT);
    }

}
