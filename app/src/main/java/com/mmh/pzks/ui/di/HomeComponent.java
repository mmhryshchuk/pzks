package com.mmh.pzks.ui.di;

import com.mmh.pzks.core.di.scope.PerActivity;
import com.mmh.pzks.ui.screens.home.HomeActivity;
import com.mmh.pzks.ui.screens.home.HomePresenter;
import com.mmh.pzks.ui.screens.home.HomePresenterImpl;

import dagger.Module;
import dagger.Provides;
import dagger.Subcomponent;

/**
 * Created by mmhryshchuk on 05.09.16.
 */
@Subcomponent(modules = HomeComponent.HomeModule.class)
@PerActivity
public interface HomeComponent {

    void inject(HomeActivity homeActivity);

    @Module
    class HomeModule{

        @Provides
        @PerActivity
        HomePresenter providePresenter(){
            return new HomePresenterImpl();
        }
    }
}
