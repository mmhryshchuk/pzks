package com.mmh.pzks.ui.screens.home;

import com.google.firebase.auth.FirebaseAuth;
import com.mmh.pzks.core.mvp.BasePresenter;

/**
 * Created by mmhryshchuk on 05.09.16.
 */
public class HomePresenterImpl extends BasePresenter<HomeView> implements HomePresenter{

    FirebaseAuth auth;

    public HomePresenterImpl() {
        auth = FirebaseAuth.getInstance();
    }

    @Override
    public void onScheduleClick() {
        if (getView() == null) return;
        getView().openSchedule();
    }

    @Override
    public void onNewClick() {

    }

    @Override
    public void onAboutClick() {
        if (getView() == null) return;
        getView().openAbout();
    }

    @Override
    public void onLogout() {
        if (getView() == null) return;
        auth.signOut();
        getView().openLogin();
    }
}
