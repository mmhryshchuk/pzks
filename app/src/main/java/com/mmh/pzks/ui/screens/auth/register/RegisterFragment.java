package com.mmh.pzks.ui.screens.auth.register;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.mmh.pzks.R;
import com.mmh.pzks.core.android.MVPBaseFragment;
import com.mmh.pzks.core.rx.SimpleSubscriber;
import com.mmh.pzks.core.rx.TextWatcherObservable;
import com.mmh.pzks.core.utils.EmailValidator;
import com.mmh.pzks.core.utils.StringUtils;
import com.mmh.pzks.domain.entities.AuthDvo;
import com.mmh.pzks.ui.screens.auth.AuthActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by mmhryshchuk on 17.08.16.
 */
public class RegisterFragment  extends MVPBaseFragment<RegisterPresenter> implements RegisterView{

    @BindView(R.id.register_fragment_email)
    EditText vEmail;
    @BindView(R.id.register_fragment_password)
    EditText vPassword;
    @BindView(R.id.register_fragment_btn_register)
    TextView vRegister;

    AuthDvo authDvo;
    CompositeSubscription subscription = new CompositeSubscription();


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.register_fragment,container,false);
        ButterKnife.bind(this,view);
        return view;
    }


    @Override
    public void onStart() {
        super.onStart();
        getPresenter().attachView(this);
        enableRegister(false);
        subscription.add(TextWatcherObservable.create(vEmail).subscribe(new SimpleSubscriber<String>(){
            @Override
            public void onNext(String item) {
                super.onNext(item);
                validate();
            }
        }));
        subscription.add(TextWatcherObservable.create(vPassword).subscribe(new SimpleSubscriber<String>(){
            @Override
            public void onNext(String item) {
                super.onNext(item);
                validate();
            }
        }));
    }

    @Override
    public void onStop() {
        super.onStop();
        getPresenter().detachView();
    }


    @Override
    public void showErrorMassage() {
        Toast.makeText(getActivity(),"Something go wrong! Don`t give up!!",Toast.LENGTH_SHORT).show();
    }

    @Override
    public void enableRegister(boolean enable) {
        vRegister.setEnabled(enable);
    }


    public void validate(){
        boolean isAllFine = true;
        String email = vEmail.getText().toString();
        String password = vPassword.getText().toString();
        if (StringUtils.isNullEmpty(email) ){
            isAllFine = false;
        }
        if (StringUtils.isNullEmpty(password) ){
            isAllFine = false;
        }
        if (!EmailValidator.isValid(email)){
            vEmail.setTextColor(ContextCompat.getColor(getActivity(), R.color.colorTextError));
            isAllFine = false;
        }else {
            vEmail.setTextColor(ContextCompat.getColor(getActivity(), R.color.colorAccent));

        }
        if (password.length() < 6){
            isAllFine = false;
            vPassword.setTextColor(ContextCompat.getColor(getActivity(), R.color.colorTextError));
        }else{
            vPassword.setTextColor(ContextCompat.getColor(getActivity(), R.color.colorAccent));
            }
        if (isAllFine){
            enableRegister(true);
        } else {
            enableRegister(false);
        }
    }

    @OnClick(R.id.register_fragment_btn_register)
    public void onRegisterClick(){
        String password = vPassword.getText().toString();
        String email  = vEmail.getText().toString();
        authDvo = new AuthDvo(email,password);
        getPresenter().onRegisterClick(authDvo, (AuthActivity) getActivity());
    }

    @OnClick(R.id.register_fragment_btn_login)
    public void onLoginClick(){
        getPresenter().onLoginClick((AuthActivity) getActivity());
    }


    @OnClick(R.id.register_fragment_btn_reset)
    public void onResetClick(){
        getPresenter().onForgotClick((AuthActivity) getActivity());
    }
}
