package com.mmh.pzks.ui.screens.about;

import com.mmh.pzks.core.mvp.Presenter;

/**
 * Created by mmhryshchuk on 05.09.16.
 */
public interface AboutPresenter extends Presenter<AboutView> {
}
