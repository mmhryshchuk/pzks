package com.mmh.pzks.ui.screens.schedule;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;

import com.mmh.pzks.R;
import com.mmh.pzks.core.android.App;
import com.mmh.pzks.core.android.BaseActivity;
import com.mmh.pzks.ui.di.ProfessorComponent;

import javax.inject.Inject;

import butterknife.ButterKnife;

/**
 * Created by mmhryshchuk on 25.08.16.
 */
public class ProfessorActivity extends BaseActivity {

    @Inject
    ProfessorPresenter professorPresenter;

    public static void start(Activity activity){
        Intent intent = new Intent(activity, ProfessorActivity.class);
        activity.startActivity(intent);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_professor);
        ButterKnife.bind(this);
        replaceFragment(professorPresenter,new ProfessorFragment());

    }


}
