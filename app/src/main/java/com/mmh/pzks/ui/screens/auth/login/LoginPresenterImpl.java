package com.mmh.pzks.ui.screens.auth.login;

import com.google.firebase.auth.FirebaseAuth;
import com.mmh.pzks.core.mvp.BasePresenter;
import com.mmh.pzks.domain.entities.AuthDvo;
import com.mmh.pzks.ui.screens.auth.AuthActivity;

/**
 * Created by mmhryshchuk on 17.08.16.
 */
public class LoginPresenterImpl extends BasePresenter<LoginView> implements LoginPresenter{

    FirebaseAuth auth;

    public LoginPresenterImpl() {
        auth = FirebaseAuth.getInstance();
    }

    @Override
    public void onLoginClick(AuthDvo authDvo, AuthActivity authActivity) {
        if (getView() == null) return;
        auth.signInWithEmailAndPassword(authDvo.getEmail(),authDvo.getPassword()).addOnCompleteListener(task -> {
            if (task.isSuccessful()){
                authActivity.openNextScreen();
            }else {
                getView().showErrorMessage();
            }
        });
    }

    @Override
    public void onRegisterClick(AuthActivity authActivity) {
        if (getView() == null) return;
        authActivity.openRegisterScreen();
    }

    @Override
    public void onResetClick(AuthActivity authActivity) {
        if (getView() == null) return;
        authActivity.openResetScreen();
    }

    @Override
    public void onAnonClick() {
        if (getView() == null) return;
        getView().openHome();
    }
}
