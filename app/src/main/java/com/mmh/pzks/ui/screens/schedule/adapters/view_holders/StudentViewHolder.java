package com.mmh.pzks.ui.screens.schedule.adapters.view_holders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.mmh.pzks.R;

import butterknife.BindView;
import butterknife.ButterKnife;


/**
 * Created by mmhryshchuk on 28.07.16.
 */
public class StudentViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.shcedule_student_group_name)
    TextView vGroupName;


    public StudentViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this,itemView);
    }

//    public void bind(Context context, CategoryDvo categoryDvo){
//        vPrimaryText.setText(categoryDvo.getPrimaryText());
//        vSecondaryText.setText(String.format("%s %s", categoryDvo.getLectionCount(), vSecondaryText.getContext().getResources().getString(R.string.category_lecture)));
//    }
}
