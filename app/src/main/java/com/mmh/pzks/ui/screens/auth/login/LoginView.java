package com.mmh.pzks.ui.screens.auth.login;

/**
 * Created by mmhryshchuk on 17.08.16.
 */
public interface LoginView {
    void showErrorMessage();
    void enableLogin(boolean enable);
    void openHome();
}
