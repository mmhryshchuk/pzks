package com.mmh.pzks.ui.screens.auth.reset;

import com.mmh.pzks.core.mvp.Presenter;
import com.mmh.pzks.domain.entities.AuthDvo;
import com.mmh.pzks.ui.screens.auth.AuthActivity;

/**
 * Created by mmhryshchuk on 17.08.16.
 */
public interface ResetPresenter extends Presenter<ResetView> {
    void onResetClick(AuthDvo authDvo, AuthActivity authActivity);

}
