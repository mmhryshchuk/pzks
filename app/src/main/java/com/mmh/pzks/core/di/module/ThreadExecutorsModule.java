package com.mmh.pzks.core.di.module;


import com.mmh.pzks.core.executors.PostExecutionThread;
import com.mmh.pzks.core.executors.ThreadExecutor;
import com.mmh.pzks.domain.executors.JobExecutor;
import com.mmh.pzks.domain.executors.UIThread;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;


/**
 * Created by vladimir on 02.06.16.
 */
@Module
public class ThreadExecutorsModule {

    @Provides
    @Singleton
    ThreadExecutor provideThreadExecuter(){
        return new JobExecutor();
    }

    @Provides
    @Singleton
    PostExecutionThread providePostExecutionThread (){
        return new UIThread();
    }


}
