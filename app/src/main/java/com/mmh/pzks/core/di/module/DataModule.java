package com.mmh.pzks.core.di.module;

import android.database.sqlite.SQLiteDatabase;

import com.mmh.pzks.core.android.App;
import com.mmh.pzks.domain.daogen.DaoMaster;
import com.mmh.pzks.domain.daogen.DaoSession;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class DataModule {

    @Provides
    @Singleton
    public DaoSession provideSession(App app){
        DaoMaster.DevOpenHelper helper = new DaoMaster.DevOpenHelper(app, "pzks",null);
        DaoMaster master = new DaoMaster(helper.getWritableDatabase());
        return master.newSession();
    }
}
