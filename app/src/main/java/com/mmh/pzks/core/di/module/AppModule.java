package com.mmh.pzks.core.di.module;

import com.mmh.pzks.core.android.App;
import com.mmh.pzks.core.bus.Bus;
import com.mmh.pzks.core.executors.PostExecutionThread;

import org.greenrobot.eventbus.EventBus;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module()
public class AppModule {

    private App app;

    public AppModule(App app) {
        this.app = app;
    }

    @Singleton
    @Provides
    public App provideAppContext(){
        return app;
    }

    @Singleton
    @Provides
    public Bus provideBus(PostExecutionThread postExecutionThread){
        return new Bus(new EventBus(), postExecutionThread);
    }
}
