package com.mmh.pzks.core.utils;

import android.os.Build;
import android.view.View;
import android.view.ViewTreeObserver;

/**
 * Created by bezhe on 24.02.2016.
 */
public class ViewUtils {
    public static void removeGlobalListeners(View target, ViewTreeObserver.OnGlobalLayoutListener listener){
        if (target != null) {
            if (Build.VERSION.SDK_INT > 16) {
                target.getViewTreeObserver()
                        .removeOnGlobalLayoutListener(listener);
            } else {
                target.getViewTreeObserver().removeGlobalOnLayoutListener(listener);
            }
        }
    }
}
