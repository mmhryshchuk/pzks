package com.mmh.pzks.core.di.component;

import com.mmh.pzks.core.android.App;
import com.mmh.pzks.core.bus.Bus;
import com.mmh.pzks.core.di.module.AppModule;
import com.mmh.pzks.core.di.module.DataModule;
import com.mmh.pzks.core.di.module.ThreadExecutorsModule;
import com.mmh.pzks.core.executors.PostExecutionThread;
import com.mmh.pzks.core.executors.ThreadExecutor;
import com.mmh.pzks.domain.daogen.DaoSession;
import com.mmh.pzks.ui.di.AboutComponent;
import com.mmh.pzks.ui.di.AuthComponent;
import com.mmh.pzks.ui.di.HomeComponent;
import com.mmh.pzks.ui.di.ProfessorComponent;
import com.mmh.pzks.ui.screens.about.AboutActivity;

import javax.inject.Singleton;

import dagger.Component;
@Singleton
@Component(modules = {AppModule.class,
        DataModule.class,
        ThreadExecutorsModule.class
})
public interface AppComponent {

    AuthComponent plus(AuthComponent.AuthModule authModule);
    ProfessorComponent plus(ProfessorComponent.ProfessorModule professorModule);
    HomeComponent plus(HomeComponent.HomeModule homeModule);
    AboutComponent plus(AboutComponent.AboutModule aboutModule);


    App getApp();
    Bus getBus();
    DaoSession getDaoSession();
    ThreadExecutor getJobExcecutor();
    PostExecutionThread getUIThread();


}
