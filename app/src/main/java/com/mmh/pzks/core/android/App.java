package com.mmh.pzks.core.android;

import android.app.Application;
import android.content.Context;

import com.mmh.pzks.core.di.component.AppComponent;
import com.mmh.pzks.core.di.component.DaggerAppComponent;
import com.mmh.pzks.core.di.module.AppModule;
import com.mmh.pzks.core.di.module.DataModule;
import com.mmh.pzks.core.di.module.ThreadExecutorsModule;

import javax.inject.Inject;

public class App extends Application {

    private AppComponent appComponent;

    public static App getApp(Context context) {
        return (App) context.getApplicationContext();
    }

    public AppComponent getAppComponent(){
        return appComponent;
    }

    @Inject
    @Override
    public void onCreate() {
        super.onCreate();
        setupAppComponent();
    }

    private void setupAppComponent(){
        appComponent = DaggerAppComponent.builder()
                .appModule(new AppModule(this))
                .dataModule(new DataModule())
                .threadExecutorsModule(new ThreadExecutorsModule())
                .build();
    }

}
