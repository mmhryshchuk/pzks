package com.mmh.pzks.core.android;

import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.widget.TextView;

import com.mmh.pzks.R;

import butterknife.ButterKnife;

/**
 * Created by vladimir on 07.06.16.
 */
public class BaseFragment extends Fragment {

    public void showProgress(){

    }

    public void hideProgress(){

    }


    protected void setupToolbar(Toolbar toolbar, String text, int icon, android.view.View.OnClickListener onNavigationClickListener){
        ((BaseActivity)getActivity()).setSupportActionBar(toolbar);
        ((BaseActivity)getActivity()).getSupportActionBar().setDisplayShowTitleEnabled(false);
        ((BaseActivity)getActivity()).getSupportActionBar().setDisplayShowHomeEnabled(true);
        TextView textView = ButterKnife.findById(toolbar, R.id.toolbar_text);
        textView.setText(text);
        toolbar.setNavigationIcon(icon);
        toolbar.setNavigationOnClickListener(onNavigationClickListener);
    }

    public void setupToolbarSecondary(Toolbar toolbar, String primaryText, String secondaryText , int icon, android.view.View.OnClickListener onNavigationClickListener){
        ((BaseActivity)getActivity()).setSupportActionBar(toolbar);
        ((BaseActivity)getActivity()).getSupportActionBar().setDisplayShowTitleEnabled(false);
        ((BaseActivity)getActivity()).getSupportActionBar().setDisplayShowHomeEnabled(true);
        TextView textViewPrimary = ButterKnife.findById(toolbar, R.id.toolbar_text_primary);
        TextView textViewSecondary = ButterKnife.findById(toolbar, R.id.toolbar_text_secondary);
        textViewPrimary.setText(primaryText);
        textViewSecondary.setText(secondaryText);
        toolbar.setNavigationIcon(icon);
        toolbar.setNavigationOnClickListener(onNavigationClickListener);
    }
}
