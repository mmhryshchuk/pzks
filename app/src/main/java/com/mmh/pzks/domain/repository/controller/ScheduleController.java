package com.mmh.pzks.domain.repository.controller;

import com.mmh.pzks.domain.daogen.Schedule;
import com.mmh.pzks.domain.repository.ScheduleRepository;

import java.util.List;

import javax.inject.Inject;

/**
 * Created by mmhryshchuk on 19.07.16.
 */

public class ScheduleController {
    @Inject
    ScheduleRepository repository;

    public Schedule findSchedule(long id){
        return repository.findScheduleById(id);
    }

    public List<Schedule> getScheduleForDay(String day){
        return repository.getScheduleForDay(day);
    }

    public List<Schedule> getAllSchedule(){
        return repository.getAllSchedules();
    }

    public void addSchedule(Schedule schedule){
        repository.addSchedule(schedule);
    }

    public void update(Schedule schedule){
        repository.update(schedule);
    }

    public void deleteAllSchedule(){
        repository.dropSchedules();
    }

    public List<Schedule> getProfessorSchedule(long id){
        return repository.getProfessorSchedule(id);
    }
}
