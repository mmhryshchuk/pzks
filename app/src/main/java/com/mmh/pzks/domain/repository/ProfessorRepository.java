package com.mmh.pzks.domain.repository;

import com.mmh.pzks.domain.daogen.DaoSession;
import com.mmh.pzks.domain.daogen.Professor;
import com.mmh.pzks.domain.daogen.ProfessorDao;

import java.util.List;

import javax.inject.Inject;

import de.greenrobot.dao.query.QueryBuilder;

/**
 * Created by mmhryshchuk on 19.07.16.
 */
public class ProfessorRepository {

    ProfessorDao professorDao;
    DaoSession daoSession;

    @Inject
    public ProfessorRepository(ProfessorDao professorDao, DaoSession daoSession) {
        this.professorDao = professorDao;
        this.daoSession = daoSession;
    }

    public Professor findProfessorByID(Long professorID){
        if (professorID == null){
            return null;
        }
        QueryBuilder<Professor> queryBuilder = professorDao.queryBuilder();
        return queryBuilder.where(ProfessorDao.Properties.Id.eq(professorID)).unique();
    }

    public List<Professor> getProfessors(List<Long> ids){
        QueryBuilder<Professor> qb = professorDao.queryBuilder();
        return qb.where(
                ProfessorDao.Properties.Id.in(ids)
        ).list();
    }

    public void update(final Professor professor){
        daoSession.runInTx(new Runnable() {
            @Override
            public void run() {
                professorDao.insertOrReplace(professor);
            }
        });
    }

    public void addProfessor(final Professor professor){
        daoSession.runInTx(new Runnable() {
            @Override
            public void run() {
                professorDao.insertOrReplace(professor);
            }
        });
    }

    public List<Professor> getAllProfessors(){
        return professorDao.loadAll();
    }

    public void dropProfessors(){
        daoSession.runInTx(new Runnable() {
            @Override
            public void run() {
                professorDao.deleteAll();
            }
        });
    }



}
