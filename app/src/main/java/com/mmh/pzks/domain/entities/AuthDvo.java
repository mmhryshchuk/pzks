package com.mmh.pzks.domain.entities;

/**
 * Created by mmhryshchuk on 17.08.16.
 */
public class AuthDvo {

    private String email;
    private String password;

    public AuthDvo() {
    }

    public AuthDvo(String email, String password) {
        this.email = email;
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
