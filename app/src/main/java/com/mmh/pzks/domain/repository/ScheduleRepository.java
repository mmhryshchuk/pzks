package com.mmh.pzks.domain.repository;

import com.mmh.pzks.domain.daogen.DaoSession;
import com.mmh.pzks.domain.daogen.Schedule;
import com.mmh.pzks.domain.daogen.ScheduleDao;

import java.util.List;

import javax.inject.Inject;

import de.greenrobot.dao.query.QueryBuilder;

/**
 * Created by mmhryshchuk on 19.07.16.
 */
public class ScheduleRepository {

    ScheduleDao scheduleDao;
    DaoSession daoSession;

    @Inject
    public ScheduleRepository(ScheduleDao scheduleDao, DaoSession daoSession) {
        this.scheduleDao = scheduleDao;
        this.daoSession = daoSession;
    }

    public Schedule findScheduleById(Long id){
        if (id == null){
            return  null;
        }

        QueryBuilder<Schedule> queryBuilder = scheduleDao.queryBuilder();
        return queryBuilder.where(ScheduleDao.Properties.Id.eq(id)).unique();
    }

    public List<Schedule> getScheduleForDay(String day){
        if (day == null){
            return null;
        }
        QueryBuilder qb = scheduleDao.queryBuilder();
        return qb.where(ScheduleDao.Properties.Schedule_day.eq(day)).list();
    }

    public List<Schedule> getAllSchedules(){
        return scheduleDao.loadAll();
    }

    public void addSchedule(final Schedule schedule){
        daoSession.runInTx(new Runnable() {
            @Override
            public void run() {
                scheduleDao.insertOrReplace(schedule);
            }
        });
    }

    public void update(final Schedule schedule){
        daoSession.runInTx(new Runnable() {
            @Override
            public void run() {
                scheduleDao.insertOrReplace(schedule);
            }
        });
    }

    public void dropSchedules(){
        daoSession.runInTx(new Runnable() {
            @Override
            public void run() {
                scheduleDao.deleteAll();
            }
        });
    }

    public List<Schedule> getProfessorSchedule(long id){
        QueryBuilder queryBuilder = scheduleDao.queryBuilder();
        return queryBuilder.where(ScheduleDao.Properties.Schedule_professor_id.eq(id)).list();
    }

}
