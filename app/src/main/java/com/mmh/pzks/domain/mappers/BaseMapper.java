package com.mmh.pzks.domain.mappers;

/**
 * Created by vladimir on 25.07.16.
 */
public abstract class BaseMapper<F,T> {
    public abstract T map(F f);
}
