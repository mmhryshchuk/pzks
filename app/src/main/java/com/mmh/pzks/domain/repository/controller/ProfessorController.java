package com.mmh.pzks.domain.repository.controller;

import com.mmh.pzks.domain.daogen.Professor;
import com.mmh.pzks.domain.repository.ProfessorRepository;

import java.util.List;

import javax.inject.Inject;


public class ProfessorController {

ProfessorRepository repository;
    @Inject
    public ProfessorController(){

    }


    public List<Professor> getAllProfessors(){
        return repository.getAllProfessors();
    }

    public Professor findProfessor(long id){
        return repository.findProfessorByID(id);
    }

    public void updateProfessor(Professor professor){
        repository.update(professor);
    }

    public void addProfessor(Professor professor){
        repository.addProfessor(professor);
    }

    public void deleteAllProfessors(){
        repository.dropProfessors();
    }


}
