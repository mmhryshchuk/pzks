package com.mmh.pzks.domain.daogen;

import java.util.List;
import com.mmh.pzks.domain.daogen.DaoSession;
import de.greenrobot.dao.DaoException;

// THIS CODE IS GENERATED BY greenDAO, DO NOT EDIT. Enable "keep" sections if you want to edit. 
/**
 * Entity mapped to table "PROFESSOR".
 */
public class Professor {

    private Long id;
    private String professor_name;
    private String professor_department;
    private String professor_info;
    private int professor_photo;

    /** Used to resolve relations */
    private transient DaoSession daoSession;

    /** Used for active entity operations. */
    private transient ProfessorDao myDao;

    private List<Schedule> scheduleList;

    public Professor() {
    }

    public Professor(Long id) {
        this.id = id;
    }

    public Professor(Long id, String professor_name, String professor_department, String professor_info, int professor_photo) {
        this.id = id;
        this.professor_name = professor_name;
        this.professor_department = professor_department;
        this.professor_info = professor_info;
        this.professor_photo = professor_photo;
    }

    /** called by internal mechanisms, do not call yourself. */
    public void __setDaoSession(DaoSession daoSession) {
        this.daoSession = daoSession;
        myDao = daoSession != null ? daoSession.getProfessorDao() : null;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getProfessor_name() {
        return professor_name;
    }

    public void setProfessor_name(String professor_name) {
        this.professor_name = professor_name;
    }

    public String getProfessor_department() {
        return professor_department;
    }

    public void setProfessor_department(String professor_department) {
        this.professor_department = professor_department;
    }

    public String getProfessor_info() {
        return professor_info;
    }

    public void setProfessor_info(String professor_info) {
        this.professor_info = professor_info;
    }

    public int getProfessor_photo() {
        return professor_photo;
    }

    public void setProfessor_photo(int professor_photo) {
        this.professor_photo = professor_photo;
    }

    /** To-many relationship, resolved on first access (and after reset). Changes to to-many relations are not persisted, make changes to the target entity. */
    public List<Schedule> getScheduleList() {
        if (scheduleList == null) {
            if (daoSession == null) {
                throw new DaoException("Entity is detached from DAO context");
            }
            ScheduleDao targetDao = daoSession.getScheduleDao();
            List<Schedule> scheduleListNew = targetDao._queryProfessor_ScheduleList(id);
            synchronized (this) {
                if(scheduleList == null) {
                    scheduleList = scheduleListNew;
                }
            }
        }
        return scheduleList;
    }

    /** Resets a to-many relationship, making the next get call to query for a fresh result. */
    public synchronized void resetScheduleList() {
        scheduleList = null;
    }

    /** Convenient call for {@link AbstractDao#delete(Object)}. Entity must attached to an entity context. */
    public void delete() {
        if (myDao == null) {
            throw new DaoException("Entity is detached from DAO context");
        }    
        myDao.delete(this);
    }

    /** Convenient call for {@link AbstractDao#update(Object)}. Entity must attached to an entity context. */
    public void update() {
        if (myDao == null) {
            throw new DaoException("Entity is detached from DAO context");
        }    
        myDao.update(this);
    }

    /** Convenient call for {@link AbstractDao#refresh(Object)}. Entity must attached to an entity context. */
    public void refresh() {
        if (myDao == null) {
            throw new DaoException("Entity is detached from DAO context");
        }    
        myDao.refresh(this);
    }

}
